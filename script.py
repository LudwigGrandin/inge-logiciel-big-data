import sys
from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
from pyspark.sql import HiveContext


sc = SparkContext()
sqlContext= SQLContext(sc)
hiveContext = HiveContext(sc)

crimesDetails  = Row('COG_COMMUNE_2021', 'annee', 'classe', 'uniteDeCompte', 'valeurPubliee', 'faits', 'tauxPourMille', 'complementInfoVal', 'complementInfoTaux', 'pop', 'millpop', 'LOG', 'millLOG')

crime_files = sc.textFile("file:///home/cloudera/miniprojet/ludwig/in/crimes-delits-commune.csv")
header = crime_files.first()
crime_files_noheader = crime_files.filter(lambda line: line != header)

crimes_lines = crime_files_noheader.map(lambda l: l.split(";"))

crimes = crimes_lines.map(lambda f: crimesDetails (*f))
crimes_df = hiveContext.createDataFrame(crimes)
hiveContext.sql("CREATE TABLE IF NOT EXISTS crimes2 AS SELECT * FROM crimes2")
# crimes_df.write.saveAsTable("crimes2", mode="overwrite")

print("======================Results=======================")

hiveContext.sql("SELECT classe, annee, tauxPourMille FROM crimes2").show(100)